﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour
{
    private PlayerController player;
    private Collider playerCol;

    private Animator anim;

    public GameObject PickupPrefab;
    private GameObject myPickup;

    // Start is called before the first frame update
    void Start()
    {
        var playerObj = GameObject.Find("Player");
        player = playerObj.GetComponent<PlayerController>();
        playerCol = playerObj.GetComponent<Collider>();

        anim = GetComponent<Animator>();
        anim.Play("Idle");

        myPickup = Instantiate(PickupPrefab, transform.Find("PickupPos"));
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerEnter(Collider other) {
        if (other == playerCol)
        {
            if (player.GetPickup(myPickup.name.Replace("(Clone)", "")))
            {
                Destroy(myPickup);
            }
        }
    }
}
