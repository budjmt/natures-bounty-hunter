﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TarHead : MonoBehaviour
{
    private PlayerController player;
    private Transform playerTransform;
    private Collider playerCol;

    private Animator anim;
    private Material mat;
    private NavMeshAgent agent;

    public int maxHealth = 50;
    [SerializeField]
    private int health;

    public float dmgFlashTime = 0.1f;
    private float dmgTime;

    public float maxDecayPeriod = 10;
    private float decayPeriod = 0;

    private bool pursuingPlayer = false;

    // Start is called before the first frame update
    void Start()
    {
        var p = GameObject.Find("Player");
        player = p.GetComponent<PlayerController>();
        playerTransform = p.transform;
        playerCol = p.GetComponent<Collider>();

        anim = GetComponent<Animator>();
        anim.Play("Idle");

        mat = GetComponentInChildren<Renderer>().material;

        agent = GetComponent<NavMeshAgent>();

        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (ShouldPursuePlayer())
        {
            if (!pursuingPlayer)
            {
                anim.Play("Alert");
                pursuingPlayer = true;
            }
            decayPeriod = 0;
        }
        else if (pursuingPlayer)
        {
            if (decayPeriod > maxDecayPeriod)
            {
                pursuingPlayer = false;
                anim.Play("Idle");
            }
            else
            {
                decayPeriod += Time.deltaTime;
            }
        }

        agent.isStopped = !pursuingPlayer;

        var animState = anim.GetCurrentAnimatorStateInfo(0);
        if (animState.IsName("Pursuit"))
        {
            if ((agent.destination - playerTransform.position).sqrMagnitude > 5 * 5)
            {
                agent.destination = playerTransform.position;
            }
        }

        if (Time.time - dmgTime > dmgFlashTime) {
            mat.color = Color.white;
        }
    }

    bool ShouldPursuePlayer() {
        var pos = transform.position;
        var toPlayer = playerTransform.position - pos;
        if (toPlayer.sqrMagnitude > 100 * 100) {
            return false;
        }

        RaycastHit hit;
        if (Physics.Raycast(pos, toPlayer, out hit)) {
            return hit.transform == playerTransform;
        }

        return false;
    }

    void Damage(int amt) {
        health -= amt;
        mat.color = Color.red;
        dmgTime = Time.time;
        if (health <= 0) {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision col) {
        var other = col.collider;
        if (other == playerCol) {
            player.Damage(10);
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.name.Contains("projectile"))
        {
            Damage(5);
            Destroy(other);
        }
    }
}
