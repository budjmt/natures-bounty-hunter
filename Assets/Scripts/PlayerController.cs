﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed = 10.0f;

    public float jumpSpeed = 10.0f;
    public float jumpCooldown = 2;
    private float currentJumpCooldown = 0;

    [SerializeField]
    public float mouseSensitivity = 5.0f;
    [SerializeField]
    public float mouseSmoothing = 2.0f;

    [SerializeField]
    public float joySensitivity = 5.0f;
    [SerializeField]
    public float joySmoothing = 2.0f;

    private Rigidbody rigidBody;

    // child camera transform
    private Transform cam;

    // Gun animator
    private GunScript gunScript;
    private Animator gunAnim;

    public Transform ammoArrow;
    private Quaternion ammoArrowInitRotation;
    public int maxAmmo = 50;
    [SerializeField]
    private int ammo;
    private bool isReloading = false;

    public int maxHealth = 50;
    [SerializeField]
    private int health;
    public Image healthIcon;
    public Image damageOverlay;

    private float maxDmgCooldown = 1;
    private float dmgCooldown = 0;

    public GameObject basicBulletPrefab;
    public float bulletSpeed = 100.0f;

    // get the incremental value of camera moving
    private Vector2 look;
    // smooth the mouse moving
    private Vector2 smoothV;

    // Start is called before the first frame update
    void Start()
    {
        // turn off the cursor
        //Cursor.lockState = CursorLockMode.Locked;

        rigidBody = GetComponent<Rigidbody>();
        cam = transform.Find("Camera");
        
        var gun = transform.Find("Camera/Gun");
        //gun.GetComponentInChildren<Renderer>().material.renderQueue = 40000;

        gunAnim = gun.GetComponent<Animator>();
        gunScript = gun.GetComponent<GunScript>();
        gunScript.bulletPrefab = basicBulletPrefab;
        gunScript.bulletSpeed = bulletSpeed;
        gunScript.player = this;

        ammoArrowInitRotation = ammoArrow.localRotation;
        Reload(maxAmmo);

        AddHealth(maxHealth);
    }

    // Update is called once per frame
    void Update()
    {
        ControlMove();
        ControlLook();
        ControlFire();
        UpdateMisc();
    }

    public bool GetPickup(string pickup) {
        switch (pickup) {
            case "Health":
                if (health < maxHealth)
                {
                    this.AddHealth(10);
                    return true;
                }
                break;
            case "Ammo":
                if (ammo < maxAmmo)
                {
                    Reload(10);
                    return true;
                }
                break;
        }
        return false;
    }

    void UpdateMisc() {
        dmgCooldown -= Time.deltaTime;
        dmgCooldown = Mathf.Max(0, dmgCooldown);
        damageOverlay.color = new Color(Color.red.r, Color.red.g, Color.red.b, (dmgCooldown / maxDmgCooldown) - 0.6f);
    }

    void AddHealth(int amt) {
        health += amt;
        health = Mathf.Clamp(health, 0, maxHealth);
        var t = healthIcon.transform;
        var ratio = (float)health / maxHealth;
        t.localScale = new Vector3(ratio, t.localScale.y, t.localScale.z);
        healthIcon.color = new Color(1 - ratio, ratio + 0.25f, 0);
    }

    public void Damage(int amt) {
        AddHealth(-amt);
        dmgCooldown = maxDmgCooldown;
    }

    public bool IsAlive() {
        return health > 0;
    }

    void AddAmmo(int amt) {
        ammo += amt;
        ammo = Mathf.Clamp(ammo, 0, maxAmmo);
        ammoArrow.Rotate(0, 0, 360 * amt / maxAmmo);
    }

    public bool TryRemoveAmmo(int amt) {
        if (ammo < amt) {
            return false;
        }

        AddAmmo(-amt);
        return true;
    }

    void Reload(int amt) {
        ammo += amt;
        ammoArrow.localRotation = ammoArrowInitRotation;
        gunAnim.Play("Reload");
        isReloading = true;
    }

    void ControlFire()
    {
        if (isReloading) {
            var animState = gunAnim.GetCurrentAnimatorStateInfo(0);
            if (animState.IsName("Idle"))
            {
                isReloading = false;
            }
        }
        else if (Input.GetAxis("Fire1") > 0)
        {
            gunAnim.Play("FastFire");
        }
    }

    void ControlMove()
    {
        var translation = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        var strafe = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        transform.Translate(strafe, 0, translation);

        currentJumpCooldown -= Time.deltaTime;

        if (Input.GetAxis("Jump") > 0 && currentJumpCooldown <= 0)
        {
            rigidBody.velocity = new Vector3(0, jumpSpeed, 0);
            currentJumpCooldown = jumpCooldown;
        }

        //if (Input.GetKeyDown("escape"))
        //{
        //    // turn on the cursor
        //    Cursor.lockState = CursorLockMode.None;
        //}
    }

    void ControlLook()
    {
        var mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        var joyDelta = new Vector2(Input.GetAxis("Joy X"), Input.GetAxis("Joy Y"));

        var delta = mouseDelta;
        var sensitivity = mouseSensitivity;
        var smoothing = mouseSmoothing;
        if (Mathf.Abs(joyDelta.x) > .05 || Mathf.Abs(joyDelta.y) > .05)
        {
            delta = joyDelta;
            sensitivity = joySensitivity;
            smoothing = joySmoothing;
        }

        delta = Vector2.Scale(delta, new Vector2(sensitivity * smoothing, sensitivity * smoothing));

        // the interpolated float result between the two float values
        smoothV.x = Mathf.Lerp(smoothV.x, delta.x, 1f / smoothing);
        smoothV.y = Mathf.Lerp(smoothV.y, delta.y, 1f / smoothing);
        // incrementally add to the camera look
        look += smoothV;

        cam.localRotation = Quaternion.AngleAxis(-look.y, Vector3.right);
        transform.localRotation = Quaternion.AngleAxis(look.x, transform.up);
    }
}