﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{
    private Transform gunTip;

    public PlayerController player;

    public GameObject bulletPrefab;
    public float bulletSpeed;

    // Start is called before the first frame update
    void Start()
    {
        gunTip = transform.Find("GunTip");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FireProjectile() {
        if (!player.TryRemoveAmmo(1))
        {
            return;
        }

        var bullet = Instantiate(bulletPrefab);
        bullet.transform.position = gunTip.position;
        bullet.transform.forward = gunTip.forward;
        bullet.GetComponent<Rigidbody>().velocity = gunTip.forward * bulletSpeed;
        Destroy(bullet, 2);
    }
}
