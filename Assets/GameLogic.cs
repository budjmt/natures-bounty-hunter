﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour
{
    public GameObject enemyPrefab;
    public PlayerController player;
    public Text timeDisplay;

    private float timeTilNextWave = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeTilNextWave -= Time.deltaTime;
        if (timeTilNextWave <= 0)
        {
            for (int i = Random.Range(1, 5); i > 0; --i) {
                var enemy = Instantiate(enemyPrefab);
                enemy.transform.position = new Vector3(Random.Range(5, 95), 5, Random.Range(5, 95));
            }
            timeTilNextWave = 15;
        }

        if (player.IsAlive()) {
            timeDisplay.text = "Score: " + (int) (Time.realtimeSinceStartup * 10) * 10;
        }
    }
}
